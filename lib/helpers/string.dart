const String labelEmail = 'Email';
const String labelPassword = 'Password';
const String labelLoginButton = 'MASUK';
const String labelRegisterButton = 'DAFTAR';
const String labelConfirmationButton = 'KONFIRMASI';
const String labelAddressYourHome = 'Alamat Rumah Anda';
const String labelButtonChange = 'Ganti';
const String labelButtonEdit = 'Edit';
const String labelLogout = 'Keluar';
const String labelForgot = 'Lupa Password ?';
const String labelPhoneButton = 'LANJUT';

///Profile
const String labelProfile = 'Profil';
const String labelEditProfile = 'Edit Profil';
const String labelAssociateFB = 'Kaitkan Akun';
const String labelLanguage = 'Pilihan Bahasa';
const String labelTermsOfUse = 'Syarat Penggunaan';
const String labelPrivacyPolicy = 'Kebijakan Privasi';
const String labelAboutUs = 'Tentang Kami';

const String labelCurrentLocation = 'Kamu sedang di :';
const String labelAddress = 'Alamat';
const String labelDate = 'Tanggal';
const String labelTotal = 'Total';
const String labelStatus = 'Status';
const String labelTransactionList = 'Daftar Transaksi';
const String labelShoppingCode = 'Kode Belanja';

const String labelOnProgress = 'Menunggu Pembayaran';
const String labelFinish = 'Transaksi Selesai';
const String labelCancel = 'Dibatalkan';

const String typeEvent = 'event';
const String typeQuota = 'quota';

const String statusTransactionOnProgress = 'progress';
const String statusTransactionFinish = 'finished';
const String statusTransactionCancel = 'cancel';

const String labelHelpCenter = 'Pusat Bantuan';
const String labelHelpDetail = 'Detail Bantuan';

const String labelAllEvent = 'Semua Event';

const String labelAllPromo = 'Semua Promo';
